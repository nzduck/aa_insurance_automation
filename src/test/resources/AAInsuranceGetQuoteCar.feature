# Feature/AA Insurance: Get a Quote for Car demo
Feature: AA Insurance: Get a Quote for Car 

Background: 
	Given I go to AA Insurance website 
	And I want to by car insurance

  Scenario: Buy car insurance for my BMW 320I 2022
  1.Comprehensive policy product
  2.Private use purpose
  3.Not an AA member
  4.BMW 320I - 2022

    Given I choose type of car insurance cover is "Comprehensive"
    And I want the Policy start date is today
    And I am not an AA insurance member

    Given I click the Model Selector Tap
    And My car Year Of Manufacture is "2022"
    And The car Make is "BMW"
    And The car module is "320I"
    And The Transmission type is "Auto"
    And Click the button find your car
    And Click the button car features
    And I have no aftermarket accessories
#
    Given I don't buy car on finance
    And I use my car as "Private"

    Given My personal details as below:
      |suburb                             |street                                          |dayOfBirth | monthOfBirth   | yearOfBirth | sex  |
      |MOUNT WELLINGTON, 1060, AUCKLAND|242 Penrose Road MT Wellington, Auckland1060|28          |March            |1988         |Female|


    When I get my quote from the website
    Then The premium fee should be "$68.08" per annual

  Scenario: Buy car insurance for my BMW 320I 2022
  1.Comprehensive policy product
  2.Business use purpose
  3.Not an AA member
  4.BMW 320I - 2022

    Given I choose type of car insurance cover is "Comprehensive"
    And I want the Policy start date is today
    And I am not an AA insurance member

    Given I click the Model Selector Tap
    And My car Year Of Manufacture is "2022"
    And The car Make is "BMW"
    And The car module is "320I"
    And The Transmission type is "Auto"
    And Click the button find your car
    And Click the button car features
    And I have no aftermarket accessories
#
    Given I don't buy car on finance
    And I use my car as "Business"

    Given My personal details as below:
      |suburb                             |street                                          |dayOfBirth | monthOfBirth   | yearOfBirth | sex  |
      |MOUNT WELLINGTON, 1060, AUCKLAND|242 Penrose Road MT Wellington, Auckland1060|28          |March            |1988         |Female|


    When I get my quote from the website
    Then The premium fee should be "$93.30" per annual

  Scenario: Buy car insurance for my Nissan MARCH 2010
		1.Comprehensive policy product
		2.Private use purpose
		3.Not an AA member
		4.Nissan march - 2010

	Given I choose type of car insurance cover is "Comprehensive"
	And I want the Policy start date is today
	And I am not an AA insurance member

	Given I click the Model Selector Tap
	And My car Year Of Manufacture is "2010"
	And The car Make is "NISSAN"
	And The car module is "MARCH"
    And The Transmission type is "Auto"
    And Click the button find your car
    And Click the button car features
    And I have no aftermarket accessories
#
	Given I don't buy car on finance
	And I use my car as "Private"

	Given My personal details as below:
		|suburb                             |street                                          |dayOfBirth | monthOfBirth   | yearOfBirth | sex  |
		|MOUNT WELLINGTON, 1060, AUCKLAND|242 Penrose Road MT Wellington, Auckland1060|28          |March            |1988         |Female|


	When I get my quote from the website
	Then The premium fee should be "$26.94" per annual

  Scenario: Buy car insurance for my Nissan MARCH 2010
  1.Comprehensive policy product
  2.Business use purpose
  3.Not an AA member
  4.Nissan march - 2010

    Given I choose type of car insurance cover is "Comprehensive"
    And I want the Policy start date is today
    And I am not an AA insurance member

    Given I click the Model Selector Tap
    And My car Year Of Manufacture is "2010"
    And The car Make is "NISSAN"
    And The car module is "MARCH"
    And The Transmission type is "Auto"
    And Click the button find your car
    And Click the button car features
    And I have no aftermarket accessories
#
    Given I don't buy car on finance
    And I use my car as "Business"

    Given My personal details as below:
      |suburb                             |street                                          |dayOfBirth | monthOfBirth   | yearOfBirth | sex  |
      |MOUNT WELLINGTON, 1060, AUCKLAND|242 Penrose Road MT Wellington, Auckland1060|28          |March            |1988         |Female|


    When I get my quote from the website
    Then The premium fee should be "$36.21" per annual
